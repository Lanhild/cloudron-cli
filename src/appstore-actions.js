/* jshint node:true */

'use strict';

const assert = require('assert'),
    config = require('./config.js'),
    fs = require('fs'),
    { exit, locateManifest } = require('./helper.js'),
    manifestFormat = require('cloudron-manifestformat'),
    path = require('path'),
    readlineSync = require('readline-sync'),
    safe = require('safetydance'),
    superagent = require('superagent'),
    Table = require('easy-table'),
    util = require('util');

exports = module.exports = {
    login,
    logout,
    info,
    listVersions,
    submit,
    upload,
    revoke,
    approve,
    unpublish,
    listPublishedApps
};

const NO_MANIFEST_FOUND_ERROR_STRING = 'No CloudronManifest.json found';

function createUrl(api) {
    return config.appStoreOrigin() + api;
}

// the app argument allows us in the future to get by name or id
function getAppstoreId(appstoreId, callback) {
    if (appstoreId) {
        var parts = appstoreId.split('@');

        return callback(null, parts[0], parts[1]);
    }

    var manifestFilePath = locateManifest();

    if (!manifestFilePath) return callback('No CloudronManifest.json found');

    var manifest = safe.JSON.parse(safe.fs.readFileSync(manifestFilePath));
    if (!manifest) callback(util.format('Unable to read manifest %s. Error: %s', manifestFilePath, safe.error));

    return callback(null, manifest.id, manifest.version);
}

// takes a function returning a superagent request instance and will reauthenticate in case the token is invalid
function superagentEnd(requestFactory, callback) {
    requestFactory().end(function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode === 401) return authenticate({ error: true }, superagentEnd.bind(null, requestFactory, callback));
        if (result.statusCode === 403) return callback(new Error(result.type === 'application/javascript' ? JSON.stringify(result.body) : result.text));
        callback(error, result);
    });
}

function authenticate(options, callback) {
    if (!options.hideBanner) {
        const webDomain = config.appStoreOrigin().replace('https://api.', '');
        console.log(`${webDomain} login` + ` (If you do not have one, sign up at https://${webDomain}/console.html#/register)`);
    }

    var email = options.email || readlineSync.question('Email: ', {});
    var password = options.password || readlineSync.question('Password: ', { noEchoBack: true });

    config.setAppStoreToken(null);

    superagent.post(createUrl('/api/v1/login')).auth(email, password).send({ totpToken: options.totpToken }).end(function (error, result) {
        if (error && !error.response) exit(error);

        if (result.statusCode === 401 && result.body.message.indexOf('TOTP') !== -1) {
            if (result.body.message === 'TOTP token missing') console.log('A 2FA TOTP Token is required for this account.');

            options.totpToken = readlineSync.question('2FA token: ', {});
            options.email = email;
            options.password = password;
            options.hideBanner = true;

            return authenticate(options, callback);
        }

        if (result.statusCode !== 200) {
            console.log('Login failed.');

            options.hideBanner = true;
            options.email = '';
            options.password = '';

            return authenticate(options, callback);
        }

        config.setAppStoreToken(result.body.accessToken);

        console.log('Login successful.');

        if (typeof callback === 'function') callback();
    });
}

function login(options) {
    authenticate(options);
}

function logout() {
    config.setAppStoreToken(null);
    console.log('Done.');
}

function info(options) {
    getAppstoreId(options.appstoreId, function (error, id, version) {
        if (error) exit(error);

        superagentEnd(function () {
            return superagent.get(createUrl('/api/v1/developers/apps/' + id + '/versions/' + version)).query({ accessToken: config.appStoreToken() });
        }, function (error, result) {
            if (error && !error.response) exit(util.format('Failed to list apps: %s', error.message));
            if (result.statusCode !== 200) exit(util.format('Failed to list apps: %s message: %s', result.statusCode, result.text));

            var manifest = result.body.manifest;
            console.log('id: %s', manifest.id);
            console.log('title: %s', manifest.title);
            console.log('tagline: %s', manifest.tagline);
            console.log('description: %s', manifest.description);
            console.log('website: %s', manifest.website);
            console.log('contactEmail: %s', manifest.contactEmail);
        });
    });
}

function listVersions(options) {
    getAppstoreId(options.appstoreId, function (error, id) {
        if (error) exit(error);

        superagentEnd(function () {
            return superagent.get(createUrl('/api/v1/developers/apps/' + id + '/versions')).query({ accessToken: config.appStoreToken() });
        }, function (error, result) {
            if (error && !error.response) exit(util.format('Failed to list versions: %s', error.message));
            if (result.statusCode === 404) exit('This app is not listed in appstore');
            if (result.statusCode !== 200) exit(util.format('Failed to list versions: %s message: %s', result.statusCode, result.text));

            if (result.body.versions.length === 0) return console.log('No versions found.');

            if (options.raw) return console.log(JSON.stringify(result.body.versions, null, 2));

            var versions = result.body.versions.reverse();

            // var manifest = versions[0].manifest;
            var t = new Table();

            versions.forEach(function (version) {
                t.cell('Version', version.manifest.version);
                t.cell('Creation Date', version.creationDate);
                t.cell('Image', version.manifest.dockerImage);
                t.cell('Publish state', version.publishState);
                t.newRow();
            });

            console.log();
            console.log(t.toString());
        });
    });
}

function addApp(manifest, baseDir, callback) {
    assert(typeof manifest === 'object');
    assert(typeof baseDir === 'string');
    assert(typeof callback === 'function');

    superagentEnd(function () {
        return superagent.post(createUrl('/api/v1/developers/apps'))
            .query({ accessToken: config.appStoreToken() })
            .send({ id: manifest.id });
    }, function (error, result) {
        if (error && !error.response) return exit(util.format('Failed to create app: %s', error.message));
        if (result.statusCode !== 201 && result.statusCode !== 409) {
            return exit(util.format('Failed to create app: %s message: %s', result.statusCode, result.text));
        }

        if (result.statusCode === 201) {
            console.log('New application added to the appstore with id %s.', manifest.id);
        }

        callback();
    });
}

function parseChangelog(file, version) {
    var changelog = '';
    var data = safe.fs.readFileSync(file, 'utf8');
    if (!data) return null;
    var lines = data.split('\n');

    version = version.replace(/-.*/, ''); // remove any prerelease

    for (var i = 0; i < lines.length; i++) {
        if (lines[i] === '[' + version + ']') break;
    }

    for (i = i + 1; i < lines.length; i++) {
        if (lines[i] === '') continue;
        if (lines[i][0] === '[') break;

        changelog += lines[i] + '\n';
    }

    return changelog;
}

function addVersion(manifest, baseDir, callback) {
    assert.strictEqual(typeof manifest, 'object');
    assert.strictEqual(typeof baseDir, 'string');

    var iconFilePath = null;
    if (manifest.icon) {
        var iconFile = manifest.icon; // backward compat
        if (iconFile.slice(0, 7) === 'file://') iconFile = iconFile.slice(7);

        iconFilePath = path.isAbsolute(iconFile) ? iconFile : path.join(baseDir, iconFile);
        if (!fs.existsSync(iconFilePath)) return callback(new Error('icon not found at ' + iconFilePath));
    }

    if (manifest.description.slice(0, 7) === 'file://') {
        var descriptionFilePath = manifest.description.slice(7);
        descriptionFilePath = path.isAbsolute(descriptionFilePath) ? descriptionFilePath : path.join(baseDir, descriptionFilePath);
        manifest.description = safe.fs.readFileSync(descriptionFilePath, 'utf8');
        if (!manifest.description && safe.error) return callback(new Error('Could not read/parse description ' + safe.error.message));
        if (!manifest.description) return callback(new Error('Description cannot be empty'));
    }

    if (manifest.postInstallMessage && manifest.postInstallMessage.slice(0, 7) === 'file://') {
        var postInstallFilePath = manifest.postInstallMessage.slice(7);
        postInstallFilePath = path.isAbsolute(postInstallFilePath) ? postInstallFilePath : path.join(baseDir, postInstallFilePath);
        manifest.postInstallMessage = safe.fs.readFileSync(postInstallFilePath, 'utf8');
        if (!manifest.postInstallMessage && safe.error) return callback(new Error('Could not read/parse postInstall ' + safe.error.message));
        if (!manifest.postInstallMessage) return callback(new Error('PostInstall file specified but it is empty'));
    }

    if (manifest.changelog.slice(0, 7) === 'file://') {
        var changelogPath = manifest.changelog.slice(7);
        changelogPath = path.isAbsolute(changelogPath) ? changelogPath : path.join(baseDir, changelogPath);
        manifest.changelog = parseChangelog(changelogPath, manifest.version);
        if (!manifest.changelog) return callback(new Error('Bad changelog format or missing changelog for this version'));
    }

    superagentEnd(function () {
        var req = superagent.post(createUrl('/api/v1/developers/apps/' + manifest.id + '/versions'));
        req.query({ accessToken: config.appStoreToken() });
        if (iconFilePath) req.attach('icon', iconFilePath);
        req.attach('manifest', Buffer.from(JSON.stringify(manifest)), 'manifest');
        return req;
    }, function (error, result) {
        if (error && !error.response) return callback(new Error(util.format('Failed to publish version: %s', error.message)));
        if (result.statusCode === 409) return callback('This version already exists. Use --force to overwrite.');
        if (result.statusCode !== 204) return callback(new Error(util.format('Failed to publish version (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text)));

        callback();
    });
}

function updateVersion(manifest, baseDir, callback) {
    assert.strictEqual(typeof manifest, 'object');
    assert.strictEqual(typeof baseDir, 'string');

    var iconFilePath = null;
    if (manifest.icon) {
        var iconFile = manifest.icon; // backward compat
        if (iconFile.slice(0, 7) === 'file://') iconFile = iconFile.slice(7);

        iconFilePath = path.isAbsolute(iconFile) ? iconFile : path.join(baseDir, iconFile);
        if (!fs.existsSync(iconFilePath)) return callback(new Error('icon not found at ' + iconFilePath));
    }

    if (manifest.description.slice(0, 7) === 'file://') {
        var descriptionFilePath = manifest.description.slice(7);
        descriptionFilePath = path.isAbsolute(descriptionFilePath) ? descriptionFilePath : path.join(baseDir, descriptionFilePath);
        manifest.description = safe.fs.readFileSync(descriptionFilePath, 'utf8');
        if (!manifest.description) return callback(new Error('Could not read description ' + safe.error.message));
    }

    if (manifest.postInstallMessage && manifest.postInstallMessage.slice(0, 7) === 'file://') {
        var postInstallFilePath = manifest.postInstallMessage.slice(7);
        postInstallFilePath = path.isAbsolute(postInstallFilePath) ? postInstallFilePath : path.join(baseDir, postInstallFilePath);
        manifest.postInstallMessage = safe.fs.readFileSync(postInstallFilePath, 'utf8');
        if (!manifest.postInstallMessage) return callback(new Error('Could not read/parse postInstall ' + safe.error.message));
    }

    if (manifest.changelog.slice(0, 7) === 'file://') {
        var changelogPath = manifest.changelog.slice(7);
        changelogPath = path.isAbsolute(changelogPath) ? changelogPath : path.join(baseDir, changelogPath);
        manifest.changelog = parseChangelog(changelogPath, manifest.version);
        if (!manifest.changelog) return callback(new Error('Could not read changelog or missing version changes'));
    }

    superagentEnd(function () {
        var req = superagent.put(createUrl('/api/v1/developers/apps/' + manifest.id + '/versions/' + manifest.version));
        req.query({ accessToken: config.appStoreToken() });
        if (iconFilePath) req.attach('icon', iconFilePath);
        req.attach('manifest', Buffer.from(JSON.stringify(manifest)), 'manifest');
        return req;
    }, function (error, result) {
        if (error && !error.response) return callback(new Error(util.format('Failed to publish version: %s', error.message)));
        if (result.statusCode !== 204) {
            return callback(new Error(util.format('Failed to publish version (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text)));
        }

        callback();
    });
}

function delVersion(manifest, force) {
    assert(typeof manifest === 'object');
    assert(typeof force === 'boolean');

    if (!force) {
        console.log('This will delete the version %s of app %s from the appstore!', manifest.version, manifest.id);
        var reallyDelete = readlineSync.question(util.format('Really do this? [y/N]: '), {});
        if (reallyDelete.toUpperCase() !== 'Y') exit();
    }

    superagentEnd(function () {
        return superagent.del(createUrl('/api/v1/developers/apps/' + manifest.id + '/versions/' + manifest.version)).query({ accessToken: config.appStoreToken() });
    }, function (error, result) {
        if (error && !error.response) return exit(util.format('Failed to unpublish version: %s', error.message));
        if (result.statusCode !== 204) exit(util.format('Failed to unpublish version (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));

        console.log('version unpublished.');
    });
}

function revokeVersion(appstoreId, version) {
    assert.strictEqual(typeof appstoreId, 'string');
    assert.strictEqual(typeof version, 'string');

    superagentEnd(function () {
        return superagent.post(createUrl('/api/v1/developers/apps/' + appstoreId + '/versions/' + version + '/revoke'))
            .query({ accessToken: config.appStoreToken() })
            .send({ });
    }, function (error, result) {
        if (error && !error.response) return exit(util.format('Failed to revoke version: %s', error.message));
        if (result.statusCode !== 200) exit(util.format('Failed to revoke version (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));

        console.log('version revoked.');
    });
}

function approveVersion(appstoreId, version) {
    assert.strictEqual(typeof appstoreId, 'string');
    assert.strictEqual(typeof version, 'string');

    superagentEnd(function () {
        return superagent.post(createUrl('/api/v1/developers/apps/' + appstoreId + '/versions/' + version + '/approve'))
            .query({ accessToken: config.appStoreToken() })
            .send({ });
    }, function (error, result) {
        if (error && !error.response) return exit(util.format('Failed to approve version: %s', error.message));
        if (result.statusCode !== 200) exit(util.format('Failed to approve version (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));

        console.log('Approved.');
        console.log('');

        superagentEnd(function () {
            return superagent.get(createUrl('/api/v1/developers/apps/' + appstoreId + '/versions/' + version)).query({ accessToken: config.appStoreToken() });
        }, function (error, result) {
            if (error && !error.response) exit(util.format('Failed to list apps: %s', error.message));
            if (result.statusCode !== 200) exit(util.format('Failed to list apps: %s message: %s', result.statusCode, result.text));

            console.log('Changelog for forum update: ' + result.body.manifest.forumUrl);
            console.log('');
            console.log('[' + version + ']');
            console.log(result.body.manifest.changelog);
            console.log('');
        });
    });
}


function delApp(appId, force) {
    assert(typeof appId === 'string');
    assert(typeof force === 'boolean');

    if (!force) {
        console.log('This will delete app %s from the appstore!', appId);
        var reallyDelete = readlineSync.question(util.format('Really do this? [y/N]: '), {});
        if (reallyDelete.toUpperCase() !== 'Y') exit();
    }

    superagentEnd(function () {
        return superagent.del(createUrl('/api/v1/developers/apps/' + appId)).query({ accessToken: config.appStoreToken() });
    }, function (error, result) {
        if (error && !error.response) return exit(util.format('Failed to unpublish app: %s', error.message));
        if (result.statusCode !== 204) exit(util.format('Failed to unpublish app (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));

        console.log('App unpublished.');
    });
}

function submitAppForReview(manifest, callback) {
    assert(typeof manifest === 'object');
    assert(typeof callback === 'function');

    superagentEnd(function () {
        return superagent.post(createUrl('/api/v1/developers/apps/' + manifest.id + '/versions/' + manifest.version + '/submit'))
            .query({ accessToken: config.appStoreToken() })
            .send({ });
    }, function (error, result) {
        if (error && !error.response) exit(util.format('Failed to submit app for review: %s', error.message));
        if (result.statusCode === 404) {
            console.log('No version %s found. Please use %s first.', manifest.version, 'cloudron appstore upload');
            exit('Failed to submit app for review.');
        }
        if (result.statusCode !== 200) return exit(util.format('Failed to submit app (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));

        console.log('App submitted for review.');
        console.log('You will receive an email when approved.');

        callback();
    });
}

function upload(options) {
    // try to find the manifest of this project
    var manifestFilePath = locateManifest();
    if (!manifestFilePath) return exit(NO_MANIFEST_FOUND_ERROR_STRING);

    var result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit(result.error.message);

    let manifest = result.manifest;

    const sourceDir = path.dirname(manifestFilePath);
    const appConfig = config.getAppConfig(sourceDir);

    // image can be passed in options for buildbot
    const dockerImage = options.image ? options.image : appConfig.dockerImage;
    manifest.dockerImage = dockerImage;

    if (!manifest.dockerImage) exit('No docker image found, run `cloudron build` first');

    // ensure we remove the docker hub handle
    if (manifest.dockerImage.indexOf('docker.io/') === 0) manifest.dockerImage = manifest.dockerImage.slice('docker.io/'.length);

    var error = manifestFormat.checkAppstoreRequirements(manifest);
    if (error) return exit(error);

    // ensure the app is known on the appstore side
    addApp(manifest, path.dirname(manifestFilePath), function () {
        console.log(`Uploading ${manifest.id}@${manifest.version} (dockerImage: ${manifest.dockerImage}) for testing`);

        var func = options.force ? updateVersion : addVersion;

        func(manifest, path.dirname(manifestFilePath), function (error) {
            if (error) return exit(error);
        });
    });
}

function submit() {
    // try to find the manifest of this project
    var manifestFilePath = locateManifest();
    if (!manifestFilePath) return exit(NO_MANIFEST_FOUND_ERROR_STRING);

    var result = manifestFormat.parseFile(manifestFilePath);
    if (result.error) return exit(result.error.message);

    var manifest = result.manifest;

    submitAppForReview(manifest, exit);
}

function unpublish(options) {
    getAppstoreId(options.appstoreId, function (error, id, version) {
        if (error) exit(error);

        if (!version) {
            console.log('Unpublishing ' + options.app);
            delApp(options.app, !!options.force);
            return;
        }

        console.log('Unpublishing ' + id + '@' + version);
        delVersion(id, !!options.force);
    });
}

function revoke(options) {
    getAppstoreId(options.appstoreId, function (error, id, version) {
        if (error) return exit(error);

        if (!version) return exit('--appstore-id must be of the format id@version');

        console.log('Revoking ' + id + '@' + version);
        revokeVersion(id, version);
    });
}

function approve(options) {
    getAppstoreId(options.appstoreId, function (error, id, version) {
        if (error) return exit(error);

        if (!version) return exit('--appstore-id must be of the format id@version');

        console.log('Approving ' + id + '@' + version);

        approveVersion(id, version);
    });
}

// TODO currently no pagination, only needed once we have users with more than 100 apps
function listPublishedApps(options) {
    superagentEnd(function () {
        return superagent.get(createUrl('/api/v1/developers/apps?per_page=100'))
            .query({ accessToken: config.appStoreToken() })
            .send({ });
    }, function (error, result) {
        if (error && !error.response) return exit(util.format('Failed to get list of published apps: %s', error.message));
        if (result.statusCode !== 200) return exit(util.format('Failed to get list of published apps (statusCode %s): \n%s', result.statusCode, result.body && result.body.message ? result.body.message : result.text));

        if (result.body.apps.length === 0) return console.log('No apps published.');

        var t = new Table();

        result.body.apps.forEach(function (app) {
            t.cell('Id', app.id);
            t.cell('Title', app.manifest.title);
            t.cell('Latest Version', app.manifest.version);
            t.cell('Publish State', app.publishState);
            t.cell('Creation Date', new Date(app.creationDate));
            if (options.image) t.cell('Image', app.manifest.dockerImage);
            t.newRow();
        });

        console.log();
        console.log(t.toString());
    });
}
